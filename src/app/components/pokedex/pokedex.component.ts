import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { concatMap, map, mergeMap } from 'rxjs/operators';
import { Pokemon } from 'src/app/models/Pokemon';
import { PokedexService } from 'src/app/services/pokedex/pokedex.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PokedexComponent implements OnInit {

  versionGroup = [];
  generations = [];
  selectedVersion;
  selectedGeneration;
  constructor(private _pokedex: PokedexService, private route: Router) {
  }

  ds = new PokemonDataSource(this._pokedex);

  ngOnInit(): void {
    this.fillCmbVersionGroup();
    this.fillCmbGeneration();
  }

  viewDetailPokemon(value:any){
    this._pokedex.setPokemonDetail(value);
    this.route.navigate(['home/detail']);
  }

  fillCmbVersionGroup(){
    this._pokedex.findAllVersionGroup().subscribe((data: any)=>{
      this.versionGroup = data.results;
    }, err =>{
      console.log('Error',err);
    })
  }

  fillCmbGeneration(){
    this._pokedex.findAllGenerations().subscribe((data: any)=>{
      this.generations = data.results;
    }, err =>{
      console.log('Error', err);
    })
  }

  sendFilter(){
    this.ds.setFilter(this.selectedVersion);
  }


}


// data source

export class PokemonDataSource extends DataSource<Pokemon | undefined>{

  private initData: Pokemon[] = [null];

  private dataStream = new BehaviorSubject<(Pokemon | undefined) []>(this.initData);
  private susbcription = new Subscription();
  private fetchPages = new Set<number>();
  private count = 10;
  private pageSize = 8;
  private filterGroup = null;
  private generation = 'n';

  constructor(private _pokedex: PokedexService){
    super();
  }

  setFilter(filter){
    this.filterGroup = filter;
  }

  private getPageForIndex(index: number): number {
    return Math.floor(index / this.pageSize);
  }

  formatData(body: Pokemon[], page): void{

   this.initData.splice(page * this.pageSize, this.pageSize,
      ...body,
      )
    this.dataStream.next(this.initData);
  }

  connect(collectionViewer: CollectionViewer): Observable<(Pokemon| undefined)[]> {
    this.susbcription.add(collectionViewer.viewChange.subscribe((range)=>{
      const startPage = this.getPageForIndex(range.start);
      const endPage = this.getPageForIndex(range.end -1);
      for (let i = startPage; i <= endPage; i++) {
        this.fetchPage(i);
      }

    }))
    return this.dataStream;
  }
  disconnect(): void {
    this.susbcription.unsubscribe();
  }

  fetchPage(page: number){
    if(this.fetchPages.has(page)){
      return;
    } else{
      this.fetchPages.add(page);
   this._pokedex.findAllPokemonPaginate((page *this.count + this._pokedex.codeGeneration),this.count).subscribe((data: any)=>{
        let p: Pokemon[];
        p = data.results;
        p.map(pokemon =>{
          this._pokedex.findByUrl(pokemon.url).subscribe((data:any) =>{
              pokemon.detail = data;
          }, err =>{
            console.error(err);
          })
        });
        this.formatData(p, page);
    });

    }

  }

}
