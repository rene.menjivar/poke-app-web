import {  NgModule } from '@angular/core';
import { PokedexComponent } from './pokedex/pokedex.component';
import { HeaderComponent } from './header/header.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatSelectModule} from '@angular/material/select';
import { PokedexService } from '../services/pokedex/pokedex.service';
import { CommonModule } from '@angular/common';
import { MapToIterablePipe } from '../shared/pipes/map-to-iterable.pipe';
import { PokedexGenComponent } from './pokedex-gen/pokedex-gen.component';
import { ModalComponent } from './modal/modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ItemDetailComponent } from './item-detail/item-detail.component';

@NgModule({
  imports: [CommonModule,MatToolbarModule, MatCardModule,ScrollingModule, MatSelectModule,MatDialogModule],
  exports: [PokedexComponent, HeaderComponent, PokedexGenComponent, ModalComponent],
  declarations: [PokedexComponent, HeaderComponent, MapToIterablePipe, PokedexGenComponent, ModalComponent, ItemDetailComponent],
  providers: [PokedexService],
})
export class ComponentsModule { }
