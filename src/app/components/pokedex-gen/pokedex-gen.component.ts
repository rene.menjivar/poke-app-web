
import { Component, Input, OnInit } from '@angular/core';
import { from, Observable } from 'rxjs';
import {concatMap, switchMap} from 'rxjs/operators';
import { Pokemon } from 'src/app/models/Pokemon';
import { PokedexService } from 'src/app/services/pokedex/pokedex.service';

@Component({
  selector: 'app-pokedex-gen',
  templateUrl: './pokedex-gen.component.html',
  styleUrls: ['./pokedex-gen.component.css']
})
export class PokedexGenComponent implements OnInit {

  @Input()
  codeGeneration: number;

  ds: Pokemon[];
  offset: any;
  limit: any;
  flag = false;
  constructor(private _pokedex: PokedexService) {
    console.log('entra');
  }


  ngOnInit(): void {
    this.countOffsetLimit()
  }

  concatAll(ids: any[]){
       return from(ids).pipe(concatMap((id:number) => <Observable<any>>this._pokedex.findByIdGeneration(id)))
  }

  getAll(){
    let ids =[];
    if(this.codeGeneration != 0){
      for(let i = 1; i <= this.codeGeneration-1; i++){
       ids.push(i);
      }
      this.concatAll(ids).subscribe((data:any)=>{
        let offset = 0;
        offset += data.pokemon_species.length;
        console.log(offset);

       this._pokedex.findByIdGeneration(this.codeGeneration).subscribe((resp:any)=>{
          this.limit = resp.pokemon_species.length;
          console.log('limit', this.limit);
          this._pokedex.findAllPokemonPaginate(offset, this.limit).subscribe((resp:any)=>{
            this.ds = resp.results;
            console.log(this.ds);
            this.ds.map(pokemon =>{
              this._pokedex.findByUrl(pokemon.url).subscribe((data:any)=>{
                pokemon.detail = data;
                this.flag = true;
              })
            })
          })
        })

      })
    }
  }

  countOffsetLimit(){
    if(this.codeGeneration != 0){
      for(let i = 1; i <= this.codeGeneration-1; i++){
        this._pokedex.findByIdGeneration(i).subscribe((resp:any)=>{
         this.offset += resp.pokemon_species.length;
         console.log('offset',this.offset);
        })
      }
      this._pokedex.findByIdGeneration(this.codeGeneration).subscribe((resp:any)=>{
        this.limit = resp.pokemon_species.length;
        console.log('limit', this.limit);
        this._pokedex.findAllPokemonPaginate(this.offset, this.limit).subscribe((resp:any)=>{
          this.ds = resp.results;
          console.log(this.ds);
          this.ds.map(pokemon =>{
            this._pokedex.findByUrl(pokemon.url).subscribe((data:any)=>{
              pokemon.detail = data;
              this.flag = true;
            })
          })
        })
      })
    }

  }

}
