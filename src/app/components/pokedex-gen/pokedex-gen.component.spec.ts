import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokedexGenComponent } from './pokedex-gen.component';

describe('PokedexGenComponent', () => {
  let component: PokedexGenComponent;
  let fixture: ComponentFixture<PokedexGenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokedexGenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokedexGenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
