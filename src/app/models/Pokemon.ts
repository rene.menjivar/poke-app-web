export class Pokemon {
  name: string;
  url: string;
  detail: object;
}
