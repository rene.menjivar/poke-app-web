import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatSelectModule } from '@angular/material/select';
import { ComponentsModule } from 'src/app/components/components.module';
import { HttpClienteService } from 'src/app/services/http.service';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home.routing';
import {MatGridListModule} from '@angular/material/grid-list';
import { DetailComponent } from '../detail/detail.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { ItemsComponent } from '../items/items.component';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ItemService } from 'src/app/services/pokedex/items.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import {  MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MovesService } from 'src/app/services/pokedex/moves.service';
import {MatDialogModule} from '@angular/material/dialog';
import { MovesComponent } from '../moves/moves.component';
@NgModule({
  imports: [CommonModule,
    HomeRoutingModule,
    ComponentsModule,
    MatSelectModule,
    MatGridListModule,
    MatTabsModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    MatDialogModule
  ],
  exports: [HomeComponent],
  declarations: [HomeComponent, DetailComponent, ItemsComponent,MovesComponent],
  providers: [HttpClienteService, ItemService, MovesService],
})
export class HomeModule { }
