import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';
import { MovesService } from 'src/app/services/pokedex/moves.service';
import {MatDialog} from '@angular/material/dialog';
import { ModalComponent } from 'src/app/components/modal/modal.component';

export interface Moves {
  name: string;
  type: string;
  category: string;
  details: object;
}

@Component({
  selector: 'app-moves',
  templateUrl: './moves.component.html',
  styleUrls: ['./moves.component.css']
})
export class MovesComponent implements AfterViewInit {

  moves: Moves[] = [];

  displayedColumns: string[] = [
  'name',
  'type',
  'category',
  'pp',
  'detail'
];

  dataSource: MatTableDataSource<Moves>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private _move: MovesService, public dialog: MatDialog) {
    this.dataSource = new MatTableDataSource();
    this._move.findAllMoves(813).pipe(
      map((e:any)=>{
        return e.results.map(url =>{
        return this._move.findByUrl(url.url).subscribe(res =>{
            url.detail = res;
            this.moves.push(url);
            this.dataSource._pageData(this.moves);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          });
        })
      })
    ).subscribe(res =>{
    });

    this.dataSource = new MatTableDataSource(this.moves);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(value: object) {
    this.dialog.open(ModalComponent, {
      data: value
    });
  }


}
