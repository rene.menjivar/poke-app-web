import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PokedexService } from 'src/app/services/pokedex/pokedex.service';

export interface Move{
  name: string;
  url: string;
}

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements AfterViewInit {

  pokemonDetail: object;
  chainEvolution: object;
  flagEvolution = false;

  displayedColumns: string[] = ['name'];

  dataSource: MatTableDataSource<Move>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _pokedex: PokedexService) {
    this.pokemonDetail =  this._pokedex.getPokemonDetail();
    this._pokedex.findPokemonEvolutions(this.pokemonDetail['id']).subscribe((evol:any) =>{
      if(this.pokemonDetail['name'] === evol.chain.species.name){
        this.chainEvolution = evol;
        this.flagEvolution = true;
      }else{
        console.log('no se encontro la evolucion');
      }
    });
    this.dataSource = new MatTableDataSource(this.pokemonDetail['moves']);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilterc(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
