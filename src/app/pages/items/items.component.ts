import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';
import { ItemDetailComponent } from 'src/app/components/item-detail/item-detail.component';
import { ItemService } from 'src/app/services/pokedex/items.service';

export interface Item {
  name: string;
  url: string;
  detail: object;
}


@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements AfterViewInit {

  items: Item[] = [];

  displayedColumns: string[] = ['item','name','effect_short','price', 'detail'];

  dataSource: MatTableDataSource<Item>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _item: ItemService, public dialog: MatDialog) {
    this.dataSource = new MatTableDataSource();
    this._item.findAllObjects(954).pipe(
      map((e:any)=>{
        return e.results.map(url =>{
        return this._item.findByUrl(url.url).subscribe(res =>{
            url.detail = res;
            this.items.push(url);
            this.dataSource._pageData(this.items);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          });
        })
      })
    ).subscribe(res =>{
    });

    this.dataSource = new MatTableDataSource(this.items);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(value: object) {
    this.dialog.open(ItemDetailComponent, {
      data: value
    });
  }


}
