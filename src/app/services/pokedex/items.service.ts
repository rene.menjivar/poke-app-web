import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClienteService } from '../http.service';

@Injectable()
export class ItemService {
  constructor(private _clientHttp: HttpClienteService) { }

  findAllObjects(count: number){
    let options = {
      params: new HttpParams().set('offset', '0').set('limit',count.toString())
    }
    return this._clientHttp.get('item',options);
  }

  findByUrl(url){
    return this._clientHttp.getByUrl(url,{});
  }
}
