import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClienteService } from '../http.service';

@Injectable()
export class PokedexService {

  codeGeneration: number = 0;
  pokemonDetail: object = null;

  constructor(private _clientHttp: HttpClienteService) { }


  //get and set
  getPokemonDetail(){
    return this.pokemonDetail;
  }

  setPokemonDetail(value: object){
    this.pokemonDetail = value;
  }

  // http api

  findAllPokemon(): Observable<any>{
    return this._clientHttp.get(`pokemon`,{});
  }

  findAllPokemonPaginate(offset: number,limit: number): Observable<any>{

    let options = {
      params: new HttpParams().set('offset', offset.toString()).set('limit',limit.toString())
    }
    return this._clientHttp.get(`pokemon`, options);
  }

  findAllPokemonForm(id): Observable<any>{
    return this._clientHttp.get(`pokemon-form/${id}`,{});
  }

  findByPokemon(id: number): Observable<any>{
    return this._clientHttp.get(`pokemon/${id}`,{});
  }

  findFormByPokemon(id:number): Observable<any>{
    return this._clientHttp.get(`pokemon-form/${id}`,{});
  }

  findAllVersionGroup(){
    return this._clientHttp.get(`version-group/`,{});
  }

  findAllGenerations(){
    return this._clientHttp.get(`generation/`,{});
  }

  findByIdGeneration(id: number){
    return this._clientHttp.get(`generation/${id}`,{});
  }

  findByUrl(url){
    return this._clientHttp.getByUrl(url,{});
  }

  findPokemonEvolutions(id: string){
    return this._clientHttp.get(`evolution-chain/${id}`,{});
  }


}
