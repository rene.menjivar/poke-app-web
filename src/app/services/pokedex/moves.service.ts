import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClienteService } from '../http.service';

@Injectable()
export class MovesService {
  constructor(private _clientHttp: HttpClienteService) { }

  findAllMoves(count: number){
    let options = {
      params: new HttpParams().set('offset', '0').set('limit',count.toString())
    }
    return this._clientHttp.get('move',options);
  }

  findByUrl(url){
    return this._clientHttp.getByUrl(url,{});
  }
}
