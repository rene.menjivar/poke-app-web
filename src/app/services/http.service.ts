import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class HttpClienteService {
  private apiURL: string;

  constructor(private _clientHttp: HttpClient) {
    this.apiURL = environment.apiURL;
  }

  get(path: string, options: object){
    return this._clientHttp.get(this.apiURL + path, options);
  }

  post(path: string, data: any, options: object){
    return this._clientHttp.post(this.apiURL + path, data, options);
  }

  update(path: string, data: any, options: object){
    return this._clientHttp.put(this.apiURL + path, data, options);
  }

  getByUrl(url: string, options){
    return this._clientHttp.get(url, options);
  }

}
